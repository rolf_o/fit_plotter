// The CSV files are output files from the FitToCSV converter utility
funcprot(0);
Sep = ',';
fit2csv_str = "fit";

OurBlue = color(75, 141, 204);
OurRed_r = 255;
OurRed_g = 56;
OurRed_b = 56;
OurRed = color(OurRed_r, OurRed_g, OurRed_b);

FONT_SIZE = 2;  // 2 = 12pt
FONT_STYLE = 6; // 6 = Sans Serif, not bold/italic

    function set_line_width(thickness)
        e = gce();
        e.children.thickness = thickness;
    endfunction

    // Get one value no matter the format
    function r = x_get_colvalue(v)
        v = strsubst(v, """", "");
        if (isnum(v)) then
            r = eval(v);
        else
            r = 0;
        end       
        // mprintf("%s %f \n", v, r);
    endfunction

    function R = moving_average(A, N)
        // N = samples to average over
        [nr, nc] = size(A);
        door_function = ones(N, 1); 
        R = 1/N*conv(A, door_function);
        R = R(round(N/2):round(N/2)+nr-1);
    endfunction
    
    function R = moving_average_1toN(A, N_end)
        R = A;
    
        if (N_end > 1) then 
            for i = 1:ceil((N_end-1)/2)
                R = moving_average(R, i*2+1);
            end
        end
    endfunction
    
    function x = vpercentile(p, V)
        xmax = max(V);
        xmin = min(V);
        x = xmin + p/100*(xmax-xmin);
    endfunction
    
    // Plot data
    function plot_data(scfno, DATA, filelist, logflags)
        COLORS = ["blue", "darkorange", "red", "black"];
        if (scfno >= 0) then
            clf(scf(scfno));
            f = gcf();
            f.figure_size=[1200,600];
        end
        
        h = gce(); // get("current_entity"); //get the newly created object 
        h.font_size = FONT_SIZE;  // 2 = 12pt
        h.font_style = FONT_STYLE; // 6 = Sans Serif, not bold/italic
        
        Num = size(DATA, "c");
        Cnt = size(DATA, "r");
        mprintf("Num/Cnt = %d/%d \n", Num, Cnt);
        A_DATA = ([]);
        toi = 1;
        legstr = "";
        leg_array = ([]);
        // styles = ([OurRed, OurBlue]);
        for i = 1:Num
            if (variance(DATA(:, i)) <> 0)
                A_DATA(:, toi) = DATA(:, i);
                if (legstr <> "")
                    legstr = legstr+"@";
                end;
                leg_array(toi) = fileparts(filelist(i), "fname");
                legstr = legstr + leg_array(toi);
                mprintf("legstr = %s \n", fileparts(filelist(i), "fname"));
                styles(toi) = color(COLORS(toi));
                toi = toi + 1;
            end;
        end;
        toi = toi - 1;
        if (toi > 0) then
            mprintf("Curves to plot in window %d: %d \n", scfno, toi);
            mprintf("Legs: %s \n", legstr);
            a = get("current_axes"); //get the handle of the newly created axes
            a.ticks_format = ["%5d", "%5d"];
            // plot2d(A_DATA(:, :), style=styles, leg=legstr, logflag=logflags);
            plot2d(A_DATA(:, :), style=styles, logflag=logflags);
            legend(leg_array, 1, %f);
        else
            mprintf("ERROR: Nothing to plot in window %d \n", scfno);
        end
    endfunction
    
    // Plot titles (call right after plot_data)
    function plot_title(scfno, title_str, label_x, label_y, filelist)
        filepath = fileparts(filelist(1), "path");
        ind = strindex(filepath, "\");
        r = size(ind, "c");
        fdir = part(filelist(1), ind(r-1):ind(r)-1);
        
        a=get("current_axes");
        t=a.title;
        t.font_size=4;
        t=a.x_label;
        t.font_size=3;
        t=a.y_label;
        t.font_size=3;
        xtitle(title_str, label_x, label_y);
        xstring(0, 0, fdir);
    
        if (scfno >= 0) then
            xname(title_str); // Set name of window
    
            // global filepath;
            mprintf("Directory to write image to: %s \n", filepath);
            s = sprintf('%s%splot_%i_%s.png', filepath, fit2csv_str, scfno, title_str);
            xs2png(scfno, s);
            mprintf("Plotted %s \n", s);
            // xs2pdf(scfno, sprintf('%splot_%i.pdf', filepath, scfno));
        end
    endfunction

    // Extract values from a fit2csv file
    function [POWER, CADENCE, TEMP, LR_BALANCE, PM_TE, PM_PS] = extract_fitcsv(txt, Sep, left_only)
        [txt_rows,txt_cols] = size(txt);
        POWER =([]);
        CADENCE =([]);
        TEMP =([]);
        LR_BALANCE =([]);
        PM_TE =([]);
        PM_PS =([]);
        t_last = 1;
        t_start = 0;
        for row = 1:txt_rows
            if (txt(row) <> "") then
                // mprintf("%s\n", txt(row));
                T = tokens(txt(row), [Sep, """"]);
                if (convstr(T(1), 'l') == "data") then
                    [Tnr, Tnc] = size(T);
                    t = 0;
                    for j = 1 : Tnr
                        v = T(j);
                        if (v == "timestamp") then
                           t = strtod(T(j+1));
                           // mprintf("Timestamp = %d \n", t);
                           if ((t_start == 0) & (t > 0)) then
                               t_start = t;
                           end
                           t = t - t_start;
                        end;
                        if (t > 0) then
                            select (v)
                            case "power" then
                               // POWER(t_last:t) = get_colvalue(T(j+1));
                               POWER(t_last:t) = strtod(T(j+1));
                               if (POWER(t) < 0) then
                                   mprintf("Strange power value: %s \n", T(j+1));
                               end;
                               // mprintf("Power value = %d \n", POWER(t));
                               
                            case "cadence" then
                               CADENCE(t_last:t) = strtod(T(j+1));
                            case "temperature" then
                               TEMP(t_last:t) = strtod(T(j+1));
                            case "left_right_balance" then
                               LR_BALANCE(t_last:t) = max(0, min(360, strtod(T(j+1))));
                            case "left_torque_effectiveness" then
                               PM_TE(t_last:t) = strtod(T(j+1));
                            case "left_pedal_smoothness" then
                               PM_PS(t_last:t) = strtod(T(j+1));
                            end // select
                            t_last = t;
                        end
                    end // for j
                end // if
            end // for i
        end
    endfunction

    // Plot all files in the file list (MAIN PROCESSING HERE)
    function process_files(filelist, left_only, Sep)
        [numfiles,col] = size(filelist);
        mprintf("Number of files to compare: %d\n", numfiles);
        
        clear("CSV_FILE");
        min_len = -1;
        POWER = [];
        CADENCE = [];
        TEMP = ([]);
        LR_BALANCE = ([]);
        PM_TE = ([]);
        PM_PS = ([]);
        for i=1:numfiles
            // Read in each file
            mprintf("Reading in csv file %d:\n%s\n", i, filelist(i));
            txt = mgetl(filelist(i));
            // [POWER(:,i), CADENCE(:,i), TEMP(:,i), LR_BALANCE(:,i), PM_TE(:,i), PM_PS(:,i)] = ...
            //     extract_fitcsv(txt, Sep, left_only)
            [T_POWER, T_CADENCE, T_TEMP, T_LR_BALANCE, T_PM_TE, T_PM_PS] = ...
                extract_fitcsv(txt, Sep, left_only)

            len = size(T_POWER, "r");
            mprintf("File %d length: %d secs\n", i, len);
            // Check for empty file
            if (len == 0) then
                mprintf("No POWER data in file %s\nTrying next file...\n", filelist(i));
                continue; // Try next file
            end 

            // Update minimum length
            if (min_len < 0) then
                min_len = len;
            else
                min_len = min(min_len, len);
            end
            mprintf("Minimum len = %d\n", min_len);

            // Fill in zeros values if empty
            if (size(T_CADENCE, "r") < min_len)
                T_CADENCE = zeros(min_len,1);
            end
            if (size(T_TEMP, "r") < min_len)
                T_TEMP = zeros(min_len,1);
            end
            if (size(T_LR_BALANCE, "r") < min_len)
                T_LR_BALANCE = zeros(min_len,1);
            end
            if (size(T_PM_TE, "r") < min_len)
                T_PM_TE = zeros(min_len,1);
            end
            if (size(T_PM_PS, "r") < min_len)
                T_PM_PS = zeros(min_len,1);
            end
   
            // Put into in main matrices
            POWER(1:min_len,i) = T_POWER(1:min_len);
            CADENCE(1:min_len,i) = T_CADENCE(1:min_len);
            TEMP(1:min_len,i) = T_TEMP(1:min_len);
            LR_BALANCE(1:min_len,i) = T_LR_BALANCE(1:min_len);
            PM_TE(1:min_len,i) = T_PM_TE(1:min_len);
            PM_PS(1:min_len,i) = T_PM_PS(1:min_len);
        end
        if (min_len <= 0) then
            mprintf("Done: No data to plot\n");
            abort;
        end
        mprintf("Common minimum file length: %d secs\n", min_len);

        // Make all files same length        
        POWER      = POWER(1:min_len, :);
        CADENCE    = CADENCE(1:min_len, :);
        TEMP       = TEMP(1:min_len, :);
        LR_BALANCE = LR_BALANCE(1:min_len, :);
        PM_TE      = PM_TE(1:min_len, :);
        PM_PS      = PM_PS(1:min_len, :);

        // Zero repeated cadence values on Vector (when stopping)
        //    for i = 4:length(VEC_CAD)
        //        if (VEC_CAD(i) == 0) then
        //          a = VEC_CAD(i-1);
        //            if ((a == VEC_CAD(i-2)) & (a == VEC_CAD(i-3))) then
        //                VEC_CAD(i-2:i) = 0;
        //            end
        //        end
        //    end

        mprintf("Sum of power: %d\n", sum(POWER(:,1)));

        // Zero power & cadence if either cadence is <30 
        for j = 1:min_len
            if (max(CADENCE(j, :)) < 30) then
                for i = 1:numfiles
                    POWER(j, i) = 0;
                    CADENCE(j, i) = 0;
                end
            end
        end

        if (left_only == 1) then
            // Calcutlate left power for all dual-sided data 
            // (LRBAL = 180 for 50/50 and 360 for Right-only)
            for i=1:numfiles
                if (max(LR_BALANCE(:,i)) > 0) then
                    POWER(:,i) = POWER(:,i) .* ((360-LR_BALANCE(:,i))/180);
                end
            end
        end

        ///// NOT ACTIVE YET ///////////
        ///// NOT ACTIVE YET ///////////
        ///// NOT ACTIVE YET ///////////
        if (0 > 1) then
            for j = 2:num_files
                if (filelist(j) <> "") then 
                    // What if filelist(1) is a WASP file or something?
                    // Align the two datasets
                    [c, i] = xcorr(POWER(:,1), POWER(:,j));
                    [n, m] = max(c);
                    ofs = i(m);    
                    mprintf("Offset between datasets: %d sample(s)\n", ofs);
                    if (abs(ofs) < 30) then
                        if (ofs > 0) then
                            POWER(:,j)   = POWER(1+ofs:$,j);
                            CADENCE(:,j) = CADENCE(1+ofs:$,j);
                            PM_TE(:,j)   = PM_TE(1+ofs:$,j);
                            PM_PS(:,j)   = PM_PS(1+ofs:$,j);
                            LR_BALANCE(:,j) = LR_BALANCE(1+ofs:$,j);
                        else
                            POWER(1-ofs:$,j)   = POWER(:,j);
                            CADENCE(1-ofs:$,j) = CADENCE(:,j);
                            PM_TE(1-ofs:$,j)   = PM_TE(:,j);
                            PM_PS(1-ofs:$,j)   = PM_PS(:,j);
                            LR_BALANCE(1-ofs:$,j) = LR_BALANCE(:,j);
                        end
                    end
                end
            end
        end

        // Build left-only string
        if (left_only == 1) then
            left_only_str = "[left]";
        else
            left_only_str = "[total]";
        end; 

        // Decide moving average length
        avg_len = 5;
        if (len > 60*10) then
            avg_len = 10;
        end

        // Plot raw power
        r = 1;
        A_POWER_PLOT = ([]);
        for i = 1:numfiles
            A_POWER_PLOT(:, i) = min(2000, POWER(:, i));
        end;
        plot_data(1, A_POWER_PLOT, filelist, "nn");
        if (r < 0.98) | (r > 1.02) 
            r_str = " ** SG to Vector Tq Ratio = " + sprintf("%0.3f", r) + " **";
        else
            r_str = "";
        end            
        plot_title(1, "Power "+left_only_str+r_str, "Time [Sec]", "Power [Watt]", filelist);

        // Plot power averages
        clear("A_POWER_AVG");
        for i = 1:numfiles
            A_POWER_AVG(:, i) = moving_average_1toN(POWER(:, i), avg_len);
        end;
        plot_data(2, A_POWER_AVG, filelist, "nn");
        plot_title(2, "Average Power "+left_only_str, "Time [Sec]", "Power [Watt]", filelist);
        
        // Plot raw candence 
        plot_data(3, CADENCE, filelist, "nn");
        plot_title(3, "Cadence", "Time [Sec]", "Cadence [RPM]", filelist);
        
        // Plot candence averages
        A_CAD_AVG = ([]);
        for i = 1:numfiles
            A_CAD_AVG(:, i) = moving_average_1toN(CADENCE(:, i), avg_len);
        end;
        plot_data(4, A_CAD_AVG, filelist, "nn");
        plot_title(4, "Cadence (avg)", "Time [Sec]", "Cadence [RPM]", filelist);
                
        if (left_only == 1) then
            // Plot TE
            plot_data(6, PM_TE, filelist, "nn");
            plot_title(6, "Torque Effectiveness "+left_only_str, "Time [Sec]", "Percent [%]", filelist);
        end
        
        // Plot PS
        plot_data(7, PM_PS, filelist, "nn");
        plot_title(7, "Pedal Smoothness "+left_only_str, "Time [Sec]", "Percent [%]", filelist);
        
        // Calculate real Mean Max Power distribution
        MMP = ([]);
        for i = 1:numfiles
            if (sum(POWER(:, i)) <> 0)
                mprintf("Power data in column %d, calculating MMP\n", i);
                for pavglen = 1:len
                    max_mean = max(moving_average(POWER(:, i), pavglen));
                    MMP(pavglen, i) = max_mean;
                end
                // MMP(len, i) = 0;
            else
                MMP(:, i) = zeros(len, 1);
            end    
        end;
        plot_data(8, MMP, filelist, "ln");
        plot2d(1, 0); // Scale y-axis right
        plot_title(8, "Real Mean Max Power "+left_only_str, "Duration [Sec]", "Power [W]", filelist);
        
        // Done with individual plots, now do one big one
        if (left_only == 1) then
            NumPlots = 4;
        else
            NumPlots = 4; // Include L/R Balance
        end;        
        scfno = 10;
        clf(scf(scfno));
        f = gcf();
        f.figure_size=[1200,NumPlots*600];
        if (left_only == 1) then
            xname("ALL | Comparing LEFT side power"); // Set name of window
        else
            xname("ALL | Comparing TOTAL power"); // Set name of window
        end
        
        // Plot raw power curves
        subplot(NumPlots, 1, 1);
        plot_data(-1, A_POWER_PLOT, filelist, "nn");
        plot_title(-1, "Power Meter Data "+left_only_str, "Time [Sec]", "Power [Watt]", filelist);
        
        // Plot raw candence 
        subplot(NumPlots, 1, 2);
        plot_data(-1, CADENCE, filelist, "nn");
        plot_title(-1, "Cadence", "Time [Sec]", "Cadence [RPM]", filelist);

        // Plot PS
        subplot(NumPlots, 1, 3);
        plot_data(-1, PM_PS, filelist, "nn");
        plot_title(-1, "Pedal Smoothness "+left_only_str, "Time [Sec]", "Percent [%]", filelist);
        
        if (left_only == 1) then
            subplot(NumPlots, 1, 4);
            // Plot TE
            plot_data(-1, PM_TE, filelist, "nn");
            plot_title(-1, "Torque Effectiveness "+left_only_str, "Time [Sec]", "Percent [%]", filelist);
        else
            subplot(NumPlots, 1, 4);
            // Plot L/R balance
            plot_data(-1, ((360-LR_BALANCE)/360)*100, filelist, "nn");
            plot_title(-1, "Left/Right Balance", "Time [Sec]", "Percent Left [%]", filelist);
        end
        
        // Find first valid file

        filepath = fileparts(filelist(1), "path");
        xs2pdf(scfno, sprintf('%s%splot_all.pdf', filepath, fit2csv_str));
        mprintf("Plotted %s as pdf in %s\n", sprintf('%s%ssplot_all.pdf', filepath, fit2csv_str), filepath);

    endfunction


///// MAIN ///// ///// MAIN ///// ///// MAIN ///// ///// MAIN ///// 
///// MAIN ///// ///// MAIN ///// ///// MAIN ///// ///// MAIN ///// 

// Ask for folder to process 
basedir = fullpath(uigetdir("", "Choose a folder with a set of .csv files converted with fit2csv"));

filelist = findfiles(basedir, "*.csv");
[numfiles, none] = size(filelist);
if (numfiles == 0) then
    messagebox("Error: No .csv files in this folder ("+basedir+")", "modal", "info", ["ABORT"]);
    abort;
end

for i=1:numfiles
    filelist(i) = basedir+'\'+string(filelist(i));
end

left_only = messagebox("Are we comparing LEFT power or TOTAL power?", "modal", "info", ["LEFT [arm/spindle]" "TOTAL [spider]"]);
// 1 = LEFT
// 2 = TOTAL
if (left_only == 0) then
    mprintf("Aborted...\n");
    abort;
end

// Now process all files into one set of comparison plots
process_files(filelist, left_only, Sep);

mprintf("DONE\n");
abort;

